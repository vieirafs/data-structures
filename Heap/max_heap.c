#include <limits.h>
#include <stdio.h>
#include <stdlib.h>

#define MAX_HEAP_SIZE 14

struct MaxHeap 
{
  int *arr;
  int max_size;
  int heap_size;
};

struct MaxHeap* constructor(int);

void maxHeapify(struct MaxHeap*,int);

int parent(int);

int lfChild(int);

int rgChild(int);

int removeMax(struct MaxHeap*);

void increaseKey(struct MaxHeap*, int, int);

int getMax(struct MaxHeap*);

int curSize(struct MaxHeap*);

void deleteKey(struct MaxHeap*, int);

void insertKey(struct MaxHeap*, int);

void swap(int*, int*);

void printHeap(struct MaxHeap *); 

int main()
{
  printf("it's working");
                 
  struct MaxHeap *h = constructor(MAX_HEAP_SIZE);

  printf("Entered 6 keys: - 3, 10, 12, 8, 2, 14 \n");

  insertKey(h, 3);
  insertKey(h, 10);
  insertKey(h, 12);
  insertKey(h, 8);
  insertKey(h, 2);
  insertKey(h, 14);

  printf("The current size of heap is %d\n", curSize(h));

  printf("The current maximum element is %d\n", getMax(h));

  deleteKey(h, 2);

  printf("The current size of heap is %d\n", curSize(h));

  insertKey(h, 15);
  insertKey(h, 5);

  printf("The current size of heap is %d\n", curSize(h));
  printf("The current maximum element is %d\n", getMax(h));

  printHeap(h);
  return 0;
}

void swap(int *v1, int *v2)
{
  int temp = *v1;
  *v1 = *v2;
  *v2 = temp;
}

struct MaxHeap* constructor(int tot_size) 
{
  struct MaxHeap *mh = malloc(sizeof(struct MaxHeap));
  mh->heap_size = 0;
  mh->max_size = tot_size;
  mh->arr = (int*) malloc(sizeof(int) * tot_size);
  return mh;
}

int parent(int i)
{
  return (i -1) /2;
}

int lfChild(int i)
{
  return (2 * i + 1);
}

int rgChild(int i)
{
  return (2 * i + 2);
}

int getMax(struct MaxHeap *mh)
{
  return mh->arr[0];
}

int curSize(struct MaxHeap *mh)
{
  return mh->heap_size;
}

void insertKey(struct MaxHeap *mh, int x)
{
  if (mh->heap_size == mh->max_size)
  {
    printf("\nOverflow: Could not insert key \n");
    return;
  }

  mh->heap_size++;
  // mh.heap_size += 1;

  int i = mh->heap_size - 1;
  mh->arr[i] = x;

  while (i != 0 && mh->arr[parent(i)] < mh->arr[i])
  {
    swap(&mh->arr[i], &mh->arr[parent(i)]);
    i = parent(i);
  }
}

void increaseKey(struct MaxHeap *mh, int i, int newVal)
{
  mh->arr[i] = newVal;
  while (i != 0 && mh->arr[parent(i)] < mh->arr[i])
  {
    swap(&mh->arr[i], &mh->arr[parent(i)]);
    i = parent(i);
  }
}

int removeMax(struct MaxHeap *mh)
{
  if (mh->heap_size <= 0)
    return INT_MIN;
  if (mh->heap_size == 1)
  {
    mh->heap_size--;
    return mh->arr[0];
  }

  int root = mh->arr[0];
  mh->arr[0] = mh->arr[mh->heap_size - 1];
  mh->heap_size--;

  maxHeapify(mh, 0);

  return root;
}

void deleteKey(struct MaxHeap *mh, int i)
{
  increaseKey(mh, i, INT_MAX);
  removeMax(mh);
}

void maxHeapify(struct MaxHeap *mh, int i)
{
  int l = lfChild(i);
  int r = rgChild(i);
  int largest = i;
  if (l < mh->heap_size && mh->arr[l] > mh->arr[i])
    largest = r;
  if (r < mh->heap_size && mh->arr[r] > mh->arr[largest]) 
    largest = r; 
  if (largest != i) { 
    swap(&mh->arr[i], &mh->arr[largest]); 
    maxHeapify(mh, largest); 
  }
}

void printHeap(struct MaxHeap *heap) {
    printf("Heap: ");
    for (int i = 0; i < heap->heap_size; i++) {
        printf("%d ", heap->arr[i]);
    }
    printf("\n");
}